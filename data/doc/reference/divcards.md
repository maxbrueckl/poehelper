# Div cards


## Expensive cards

- [House of Mirrors] => Mirror of Kalandra @ [The Alluring Abyss], [The Eternal Labyrinth], [Diviner's Strongbox]
- [The Doctor] => [Headhunter] @ [Burial Chambers], [Spider Forest]
- [Beauty Through Death] => {prophecy} [The Queen's Sacrifice] @ [Vaal Temple]
- [The Fiend] => Corr. [Headhunter] @ [Shrine], [The Putrid Cloister]
- [The Immortal] => {divcard} [House of Mirrors] @ [The Hall of Grandmasters]
- [The Nurse] => {divcard} [The Doctor] @ [Shavronne's Tower], [Tower]
- [Immortal Resolve] => {prophecy} [Fated Connections] @ [Carcass], [Vault]
- [The Samurai's Eye] => [Watcher's Eye] @ [Abyssal Lich] in [Abyssal Depths]
- [Abandoned Wealth] => 3 Exalted Orbs @ [Arsenal], [Mao Kun], [Precinct]
- [The Queen] => [Atziri's Acuity] @ [Vaults of Atziri]


## A Dab of Ink

- produces (x9): [The Poet's Pen]
- drops in: [Academy], [Museum], [Scriptorium], [The Library]


## Abandoned Wealth

- produces (x5): 3 [Exalted Orb]
- drops in: [Arsenal], [Mao Kun], [Precinct]


## Beauty Through Death

- produces (x5): [The Queen's Sacrifice]
- drops in: [Vaal Temple]


## House of Mirrors

- produces (x9): [Mirror of Kalandra]
- drops in: [The Alluring Abyss]


## Hunter's Reward

- produces (x3): [The Taming]
- drops in: [Grotto], [The Putrid Cloister], [Underground River]


## Immortal Resolve

- produces (x6): [Fated Connections]
- drops in: [Carcass], [Vault]


## Mawr Blaidd

- produces (x16): [Eyes of the Greatwolf]
- drops in: [Dark Forest]


## Pride Before the Fall

- produces (x8): corrupted [Kaom's Heart]
- drops in: [Caldera], [Lava Lake], [Marshes], [The Coast], [The Karui Fortress], [The Mud Flats], [The Tidal Island], [Volcano]


## The Artist

- produces (x11): corrupted level 4 [Enhance Support]


## The Brittle Emperor

- produces (x8): corrupted [Voll's Devotion]
- drops in: [The Dried Lake]


## The Celestial Stone

- produces (x10): shaper item level 100 [Opal Ring]
- drops in: [Basilica], [Lair of the Hydra], [Terrace]


## The Doctor

- produces (x8): [Headhunter]
- drops in: [Burial Chambers], [Spider Forest]


## The Dragon's Heart

- produces (x11): corrupted level 4 [Empower Support]
- drops in: [Pit of the Chimera], [The Putrid Cloister]


## The Encroaching Darkness

- produces (x7): unique map
- drops in (almost?) all maps
- **can't** produce:
    - [The Beachhead]
    - [The Perandus Manor]
    - [The Vinktar Square]
    - [Untainted Paradise]
    

## The Enlightened

- produces (x6): level 3 [Enlighten Support]
- drops in: [Ashen Wood]


## The Ethereal

- produces (x7): 6L [Vaal Regalia]
- drops in: [The Apex of Sacrifice]


## The Fiend

- produces (x11): corrupted [Headhunter]
- drops in: [Shrine], [The Putrid Cloister]


## The Hoarder

- produces (x12): [Exalted Orb]
- drops in: [The Belly of the Beast Level 1], [The Belly of the Beast Level 2], [The Harvest], [The Rotting Core], [Vault]


## The Immortal

- produces (x10): [House of Mirrors]
- drops in: [Hall of Grandmasters]


## The Iron Bard

- produces (x9): [Trash to Treasure]
- drops in: [The Vinktar Square]


## The King's Heart

- produces (x8): [Kaom's Heart]
- drops in: [Caldera], [The Coast], [Volcano]


## The Mayor

- produces (x5): [The Perandus Manor]
- drops in: [Ivory Temple]


## The Nurse

- produces (x8): [The Doctor]
- drops in: [Shavronne's Tower], [Tower]


## The Professor

- produces (x4): [The Putrid Cloister]
- drops in: [Laboratory]


## The Queen

- produces (x16): [Atziri's Acuity]
- drops in: [Vaults of Atziri]


## The Risk

- produces (x3): [Ventor's Gamble]


## The Saint's Treasure

- produces (x10): 2 [Exalted Orb]
- drops in: [Arcade], [Arsenal], [Bazaar], [Ghetto], [Precinct], [The Grain Gate], [The Imperial Fields], [The Marketplace], [The Slums]


## The Samurai's Eye

- produces (x3): [Watcher's Eye]


## The Sephirot

- produces (x11): 10 [Divine Orb]
- drops in: [Palace], [The Upper Sceptre of God]


## The Spark and the Flame

- produces (x2): [Berek's Respite]
- drops in: [Atoll], [Maelstr÷m of Chaos]


## The Undaunted

- produces (x5): corrupted unique Nemesis item
- drops in: [Basilica], [Courthouse], [Precinct]


## The Wolf

- produces (x5): unique item with Rigwald in the name
- drops in: [Flooded Mine]


## The Wolven King's Bite

- produces (x8): [Rigwald's Quills]
- drops in: [Dark Forest], [Lair]


## Wealth and Power

- produces (x11): corrupted level 4 [Enlighten Support]
- drops in: [The Lunaris Temple Level 2]


