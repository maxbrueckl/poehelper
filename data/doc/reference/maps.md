# Maps


## Academy (T6)

- upgrade:
    - from [Maze], [Port]
    - to [Underground River]
- {divcard} [A Dab of Ink]
- items: Fingerless Silk Gloves


## Arcade (T7)

- upgrade:
    - from [Phantasmagoria], [Temple]
- {divcard} [The Saint's Treasure]
- items: Two-Toned Boots


## Arsenal (T12)

- upgrade:
    - from [Bog]
    - to [Shrine]
- {divcard} [Abandoned Wealth], [The Saint's Treasure]
- items: Blue Pearl Amulet, Two-Toned Boots


## Ashen Wood (T6)

- upgrade:
    - from [Alleyways]
    - to [Primordial Pool]
- {divcard} [The Enlightened]
- items: Gripped Gloves


## Atoll (T1)

- upgrade:
    - from [The Lost Maps]
    - to [Arid Lake], [Iceberg], [Arid Lake], [Iceberg]
- {divcard} [The Spark and the Flame]
- items: Spiked Gloves


## Basilica (T14)

- upgrade:
    - from [Caldera]
    - to [Lava Lake]
- {divcard} [The Celestial Stone], [The Undaunted]
- items: Vanguard Belt


## Bazaar (T7)

- upgrade:
    - from [Phantasmagoria]
    - to [Shore]
- {divcard} [The Saint's Treasure]
- items: Bone Helmet


## Burial Chambers (T11)

- upgrade:
    - from [Coves]
    - to [Necropolis]
- {divcard} [The Doctor]
- items: Fingerless Silk Gloves, Opal Ring


## Caldera (T13)

- upgrade:
    - from [Defiled Cathedral]
    - to [Sunken City], [Basilica]
- {divcard} [Pride Before the Fall], [The King's Heart]
- items: Vanguard Belt


## Carcass (T14)

- upgrade:
    - from [Shrine]
    - to [Primordial Blocks]
- {divcard} [Immortal Resolve]
- items: Blue Pearl Amulet, Two-Toned Boots


## Courthouse (T14)

- upgrade:
    - from [Colosseum], [Shrine]
    - to [Primordial Blocks]
- {divcard} [The Undaunted]
- items: Bone Helmet


## Dark Forest (T14)

- upgrade:
    - from [Acid Caverns], [Core], [Reef]
    - to [Tower]
- {divcard} [Mawr Blaidd], [The Wolven King's Bite]
- items: Marble Amulet, Spiked Gloves


## Flooded Mine (T1)

- upgrade:
    - to [Dungeon]
- {divcard} [The Wolf]
- items: Two-Toned Boots


## Ghetto (T7)

- upgrade:
    - from [Canyon]
    - to [Wasteland]
- {divcard} [The Saint's Treasure]
- items: Two-Toned Boots


## Grotto (T3)

- upgrade:
    - from [Arid Lake], [Iceberg]
    - to [Lighthouse]
- {divcard} [Hunter's Reward]
- items: Spiked Gloves


## Ivory Temple (T12)

- upgrade:
    - from [Factory]
    - to [Dig]
- {divcard} [The Mayor]
- items: Steel Ring, Two-Toned Boots


## Laboratory (T8)

- upgrade:
    - from [Cells], [Underground River]
    - to [Moon Temple]
- {divcard} [The Professor]
- items: Vanguard Belt


## Lair (T11)

- upgrade:
    - from [Estuary]
    - to [Castle Ruins]
- {divcard} [The Wolven King's Bite]
- items: Crystal Belt


## Lair of the Hydra (T16)

- upgrade:
    - from [Tower]
- {divcard} [The Celestial Stone]
- items: Bone Helmet, Gripped Gloves, Marble Amulet, Spiked Gloves


## Lava Lake (T15)

- upgrade:
    - from [Basilica], [Palace], [Sunken City]
    - to [Pit of the Chimera]
- {divcard} [Pride Before the Fall]
- items: Fingerless Silk Gloves, Opal Ring


## Marshes (T4)

- upgrade:
    - from [Fungal Hollow]
    - to [Underground Sea], [Jungle Valley]
- {divcard} [Pride Before the Fall]
- items: Spiked Gloves


## Museum (T9)

- upgrade:
    - from [Mineral Pools]
    - to [Coral Ruins]
- {divcard} [A Dab of Ink]
- items: Gripped Gloves


## Palace (T14)

- upgrade:
    - from [Racecourse]
    - to [Desert Spring], [Lava Lake]
- {divcard} [The Sephirot]
- items: Gripped Gloves, Opal Ring


## Pit of the Chimera (T16)

- upgrade:
    - from [Desert Spring], [Lava Lake]
- {divcard} [The Dragon's Heart]
- items: Fingerless Silk Gloves, Gripped Gloves, Opal Ring, Vanguard Belt


## Precinct (T6)

- upgrade:
    - from [Jungle Valley], [Mausoleum]
    - to [Conservatory], [Geode]
- {divcard} [Abandoned Wealth], [The Saint's Treasure], [The Undaunted]
- items: Gripped Gloves


## Scriptorium (T9)

- upgrade:
    - from [Mud Geyser]
- {divcard} [A Dab of Ink]
- items: Two-Toned Boots


## Shrine (T13)

- upgrade:
    - from [Arsenal], [Overgrown Shrine]
    - to [Carcass], [Courthouse]
- {divcard} [The Fiend]
- items: Blue Pearl Amulet, Two-Toned Boots


## Spider Forest (T10)

- upgrade:
    - from [Tropical Island], [Waste Pool]
    - to [Factory], [Chateau]
- {divcard} [The Doctor]
- items: Crystal Belt


## Terrace (T14)

- upgrade:
    - to [Summit]
- {divcard} [The Celestial Stone]
- items: Crystal Belt


## Tower (T15)

- upgrade:
    - from [Dark Forest]
    - to [Lair of the Hydra]
- {divcard} [The Nurse]
- items: Marble Amulet, Spiked Gloves


## Underground River (T7)

- upgrade:
    - from [Academy], [Haunted Mansion]
    - to [Laboratory]
- {divcard} [Hunter's Reward]
- items: Vanguard Belt


## Vaal Temple (T16)

- can be obtained randomly by corrupting a T15 map
- {divcard} [Beauty Through Death]
- items: Gripped Gloves, Marble Amulet


## Vault (T9)

- upgrade:
    - from [Promenade]
    - to [Coves]
- {divcard} [Immortal Resolve], [The Hoarder]
- items: Gripped Gloves


## Volcano (T6)

- upgrade:
    - from [Underground Sea]
    - to [Lava Chamber]
- {divcard} [Pride Before the Fall], [The King's Heart]
- items: Bone Helmet
