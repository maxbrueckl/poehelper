# Syndicate

## Aisling

-T: Veiled weapons
-F: Veiled armor
+R: +1~2 veiled mods 
-I: Veiled jewelry


## Cameria

+T: Timeworn item 
+F: Harbinger orbs
-R: Sextants
+I: Sulphite scarab


## Elreon

-T: Unique weapon
-F: Unique armor
+R: Unique jewellery
+I: Reliquary scarab 


## Gravicius

+T: Div. card stack 
-F: Random div. cards
-R: Swap div. card
+I: Div. card scarab


## Guff

-T: Chaos timed craft
-F: Essence timed craft
-R: Exa/anul timed craft
-I: Aug/alt craft


## Haku

-T: Rare items
-F: Strongboxes
-R: Quality items
+I: Strongbox scarab 


## Hillock

+T: 24/26/28% qual. to weapon 
+F: 24/26/28% qual. to armour
-R: 22/24/26% qual. to flask
-I: 25/30/35% qual. to map


## It That Fled

-T: Breach splinters
-F: Abyss jewels
+R: Upgrade breachstone
+I: Breach scarab 


## Janus

-T: Qual. currency
+F: Curr. shards
+R: Perandus coins 
+I: Perandus scarab


## Jorgin

-T: Talismans
+F: Aspect item
+R: Amulet into talisman
+I: Bestiary scarab 


## Korell

+T: Essences 
+F: Map fragments
+R: Fossils
-I: Elder scarab


## Leo

-T: Silver coins
+F: Currency
+R: Use one currency on item 
-I: Torment scarab


## Riker

+T: Pick currency
-F: Pick unique
-R: Pick veiled item
+I: Pick div. card 


## Rin

+T: Uncompleted maps 
-F: Rare maps
+R: Unique maps
+I: Carto scarab


## Tora

-T: Random item
+F: Lab enchant
+R: Gem exp 
+I: Harbinger scarab


## Vagan

-T: Rare weapons
-F: Rare armor
-R: Rare jewellry
-I: Rare jewels


## Vorici

-T: Quality gems
-F: Socket currency
+R: 1-3 White sockets
+I: Shaper scarab 


