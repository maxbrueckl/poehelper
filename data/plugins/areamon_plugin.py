"""
PoEHelper area monitor plugin.

Author: Vlad Topan (vtopan/gmail)
"""

import re
import time

from poehelper.poeh_plugins import PoehPlugin
from PySide2.QtCore import Slot


class AreaInfoPlugin(PoehPlugin):
    CFG = {
        }


    def init(self):
        self.areas = []
        self.per_area = {}
        self.api.create_plugin_panel('Area Monitor',
            """
            [L<Name>L<Count>L<Avg>L<MinMax>]<Areas>
            """)    # B<Log>
        self.button_callbacks = []
        
    
    def area_changed_handler(self, area, **info):
        """
        Handle area changes.
        """
        info['area'] = area
        info['neutral'] = info['in_town'] or info['in_hideout']
        if self.areas and not self.areas[-1]['neutral']:
            area = self.areas[-1]['area']
            delta = (info['timestamp'] - self.areas[-1]['timestamp']).total_seconds()
            if delta >= 20:
                if area not in self.per_area:
                    self.per_area[area] = (len(self.per_area), [delta])
                    delta = f'{delta:.2f}s'
                    self.panel.add_grid_cell(area, 'x 1', delta)
                    time.sleep(0.1)
                else:
                    entry = self.per_area[area]
                    entry[1].append(delta)
                    self.panel.set_grid_item(entry[0], 'Count', f'x {len(entry[1])}')
                    self.panel.set_grid_item(entry[0], 'Avg', f'avg:{sum(entry[1]) / len(entry[1]):.2f}s')
                    self.panel.set_grid_item(entry[0], 'MinMax', f'{min(entry[1]):.2f}..{max(entry[1]):.2f}s')
        self.areas.append(info)
        