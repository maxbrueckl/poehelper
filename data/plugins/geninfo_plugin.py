"""
PoEHelper general info plugin.

Author: Vlad Topan (vtopan/gmail)
"""

import glob
import os
import re

from poehelper.poeh_plugins import PoehPlugin


COLORS = {
    '+': '#0A0',
    '-': '#A00',
}
HIGHLIGHT = {
    1: '<font color=%s>%s</font>',
    2: '<b><font color=%s>%s</font></b>',
}
FMT = {
    '*': 'i',
    '**': 'b',
    '__': 'u',
}
ASTYLE = 'style="color:#C30;text-decoration:none;"'
IMAGE_CACHE = {}


def wiki_url(name):
    """
    Return the wiki URL for the given name.
    """
    return f'https://pathofexile.gamepedia.com/{name.replace(" ", "_")}'


def _find_image(api, s):
    """
    Find item image regex.sub callback.
    """
    if s in IMAGE_CACHE:
        return IMAGE_CACHE[s]
    path = f'data/rsrc/images/{s}.png'
    if os.path.isfile(path):
        img = f'<img src="{path}" title="{s}" width=16 height=16>'
    else:
        fn = api.trade_full_name(s)
        if fn:
            cat = api.trade_category(fn)
            img = api.get_image_string(cat, fn, width=16, height=16)
        else:
            return '{%s}' % s
    IMAGE_CACHE[s] = img
    return img



def custom_md_to_html(api, text):
    """
    Convert a line of custom markdown to HTML.
    """
    text = re.sub(r' (https?://(.+?))(?=\s|$)', lambda m:f' <a href="{m[1]}" {ASTYLE}>{m[2]}</a>', text)
    text = re.sub(r'(\*\*|\*|__)(\S.*?\S)(\1)', lambda m:f'<{FMT[m[1]]}>{m[2]}</{FMT[m[1]]}>', text)
    text = re.sub(r'\[([\w \'-]+)\]', lambda m:f'<a href="{wiki_url(m[1])}" {ASTYLE}>{m[1]}</a>', text)
    text = re.sub(r'\{(\w+)\}', lambda m:_find_image(api, m[1]), text)
    text = re.sub(r'^([-+]{1,2})([^ ].+)', lambda m:HIGHLIGHT[len(m[1])] % (COLORS[m[1][0]], m[2]), text, flags=re.M)
    text = re.sub(r'^[ ]+', lambda m:'&nbsp;' * len(m[0]), text, flags=re.M)
    return text


class GeneralInfoPlugin(PoehPlugin):
    EVT_FILTERS = ('game_focused',)
    CFG = {
        }


    def init(self):
        """
        Initialize the plugin.
        """
        for f in glob.glob(f'{self.paths.doc}/reference/*.md'):
            cat, panels, crt_panel = None, {}, '<desc>'
            text = open(f).read()
            text = custom_md_to_html(self.api, text)
            for line in text.split('\n'):
                line = line.rstrip()
                if not line:
                    continue
                if line[0] == '#':
                    if line[1] == '#':
                        crt_panel = line[2:].strip()
                    else:
                        cat = line[2:].strip()
                else:
                    if crt_panel not in panels:
                        panels[crt_panel] = []
                    panels[crt_panel].append(line)
            desc = '\n'.join(panels.pop('<desc>', [])) or None
            self.api.add_reference_panel(cat, panels=panels, description=desc, cols=3)


