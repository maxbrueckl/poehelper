"""
PoEHelper item info plugin.

Author: Vlad Topan (vtopan/gmail)
"""

import re

from poehelper.poeh_plugins import PoehPlugin


class ItemInfoPlugin(PoehPlugin):
    EVT_FILTERS = ('game_focused',)
    CFG = {
        }


    def ingame_ctrlc_handler(self, info, text, **kwargs):
        """
        Handle in-game Ctrl+C key presses.
        """
        res = []
        attrs = info.get('attributes', {})
        aps = float(attrs.get('Attacks per Second', 0))
        if aps:
            qual = int(attrs.get('Quality', '0').strip('+%'))
            tdps, cntdps = 0, 0
            for n in ('Physical', 'Elemental', 'Chaos'):
                dam = attrs.get(f'{n} Damage')
                if dam:
                    # elemental damage has multiple values, e.g. '1-3, 7-10'
                    dam = [int(x) for x in re.split('[-, ]+', dam)]
                    dps = (aps * sum(dam) / len(dam))
                    tdps += dps
                    cntdps += 1
                    res.append((f'{n[0]}DPS', '%.02f' % dps))
            if cntdps > 1:
                res.insert(len(res) - cntdps, ('DPS', '%.02f' % tdps))
        if res:
            self.api.log(', '.join('%s: %s' % e for e in res))
                
         