"""
PoEHelper price query plugin:
- displays a list of selected currency / fragment / scarab minimum prices
- queries minimum prices for an item on in-game Ctrl+C

Author: Vlad Topan (vtopan/gmail)
"""

import re

from poehelper.poeh_plugins import PoehPlugin


# map from poe.ninja categories to app-specific item types
CAT_MAP = {
    'Currency': 'currency',
    'Fragment': 'fragment',
    'Scarab': 'scarab',
    }


class PriceQueryPlugin(PoehPlugin):
    EVT_FILTERS = ('game_focused',)
    CFG = {
        'log_short_currency_names': True,
        'watch_prices': [
            ['Scarab', 'Gilded Sulphite Scarab'],
            ['Fragment', 'Tul\'s Breachstone'],
            ['Fragment', 'Tul\'s Pure Breachstone'],
            ['Fragment', 'Esh\'s Breachstone'],
            ['Fragment', 'Esh\'s Pure Breachstone'],
            ['Currency', 'Exalted Orb'],
            ],
        }


    def init(self):
        """
        Initialize the plugin.
        """
        self.watch_items = self.cfg['watch_prices']
        self.api.create_plugin_panel('Price Watch',
            """
            B<Refresh>B<Get Lowest Offers>.
            [L<Name>L<Estim>L<Lowest>]<Items>
            """)
        for cat, name in self.watch_items:
            img = self.api.get_image_string(CAT_MAP[cat], name)
            dname = re.sub(' (Scarab|Breachstone)', lambda m:f' {m[1][0]}.', name) + ':'
            if img:
                dname = img + ' ' + dname
            self.panel.add_grid_cell(dname, '-', '-')
        self.updating = 0
        self.chaos_img = self.api.get_image_string('currency', 'Chaos Orb')
        self.panel.set_button_click('Refresh',
                lambda:self.api.run_threaded(self.update_watched_prices))
        self.panel.set_button_click('Get Lowest Offers',
                lambda:self.api.run_threaded(self.update_lowest_offers))


    def update_watched_prices(self):
        """
        Update the estimated prices in the item grid.
        """
        if self.updating:
            self.api.err('Updating already in another thread!')
            return
        self.updating = 1
        self.api.log('Querying poe.ninja...')
        pn = self.api.poeninja
        pn_prices = {}
        try:
            for i, (pncat, name) in enumerate(self.watch_items):
                # get price from poe.ninja
                if pncat not in pn_prices:
                    pn_prices[pncat] = pn.poeninja_brief_result(pn.poeninja_query_prices(pncat,
                        self.cfg['game']['league']))
                price = pn_prices[pncat].get(name, '?')
                if price != '?':
                    price = '<font color=#0A0>%s</font> %s' % (re.sub(r'\.?0+$', '', f'{price:.3f}'), self.chaos_img)
                self.panel.set_grid_item(i, 'Estim', price)
        finally:
            self.updating = 0
        self.api.log('Finished querying poe.ninja.')


    def update_lowest_offers(self):
        """
        Update the lowest offers in the item grid.
        """
        if self.updating:
            self.api.err('Updating already in another thread!')
            return
        self.updating = 1
        self.api.log('Querying pathofexile.com/trade...')
        pc = self.api.poecom
        try:
            for i, (pncat, e) in enumerate(self.watch_items):
                # get lowest prices from trade
                price = '?'
                short_name = self.api.trade_short_name(e)
                if short_name is None:
                    pass    # todo?
                else:
                    info = pc.query_exchange(short_name)
                    # .strip() because "Fractured Fossil "
                    if info and info[0]['item']['typeLine'].strip() != e.strip():
                        got = info[0]['item']['typeLine']
                        self.api.err(f'Queried for {e} / {short_name} - got {repr(got)}!')
                    else:
                        price = self.brief_trade_prices(info)
                self.panel.set_grid_item(i, 'Lowest', price)
        finally:
            self.updating = 0
        self.api.log('Finished querying trade.')


    def brief_trade_prices(self, info):
        """
        Format a trade query result as a brief list of prices.
        """
        prices = []
        prev_amt, cnt = None, 0
        for e in info:
            price = e['listing']['price']
            amt, cur = price['amount'], price['currency']
            cur_img = self.api.get_image_string('currency', self.api.trade_full_name(cur))
            amt = re.sub(r'\.?0+$', '', f'{amt:.3f}')
            cur = cur_img if cur_img else '*' + cur
            if amt == prev_amt:
                cnt += 1
            else:
                if cnt:
                    prices.append(f'{cnt}@{prev_amt}{prev_cur}')
                prev_amt = amt
                prev_cur = cur
                cnt = 1
        prices.append(f'{cnt}@{prev_amt}{cur}')
        return ", ".join(prices)


    def ingame_ctrlc_handler(self, info, text, **kwargs):
        """
        Handle in-game Ctrl+C key presses.
        """
        self.api.run_threaded(self.query_trade_asynch, (info,))


    def query_trade_asynch(self, info):
        """
        Query trade in separate thread.
        """
        trade_info = None
        pc = self.api.poecom
        if info.get('rarity', None) == 'unique':
            # unique item
            fields = {k:info[k] for k in ('name', 'corrupted', 'rarity') if info.get(k)}
            if info.get('links', 0) >= 5:
                fields['links'] = info['links']
            self.api.log('Querying trade...')
            trade_info = pc.query_trade(priced=True, **fields)
        elif set(info['types']) & {'currency', 'scarab', 'fragment', 'map'}:
            curr = info['types'][1]
            short_name = self.api.trade_short_name(curr)
            if self.cfg['log_short_currency_names'] and 'currency' in info['types'] and short_name != curr:
                self.api.log(f'Short name of {curr}: {short_name}')
            self.api.log('Querying trade...')
            trade_info = pc.query_exchange(short_name)
            # .strip() because "Fractured Fossil "
            if trade_info and trade_info[0]['item']['typeLine'].strip() != curr and 'map' not in info['types']:
                got = trade_info[0]['item']['typeLine']
                self.api.err(f'Queried for {curr} / {short_name} - got {got}!')
                return
        if trade_info:
            self.api.log(f'Lowest prices: {self.brief_trade_prices(trade_info)}')
        elif trade_info is not None:
            self.api.err(f'Not found on trade!')

