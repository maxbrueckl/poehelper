"""
Special app loader for the executable.

Author: Vlad Topan (vtopan/gmail)
"""

import cv2
import keyboard
import numpy as np 
from PIL import Image, ImageGrab
import PySide2
from PySide2 import QtGui, QtWidgets 
import requests
from tkinter import Tk, TclError

import importlib
import importlib.util as imputil 
import os
import sys


VER = '0.1.0 (ian.2019)'


# dynamically load poehelper to avoid having the source code compiled into the exe
# path = os.path.dirname(sys.argv[0])
path = os.getcwd()
sys.path.append(path)
mod = importlib.import_module('poehelper.poehelper')
#fname = rf'{path}\poehelper\poehelper.py'
#mod_name = os.path.basename(fname).rsplit('.', 1)[0]
#spec = imputil.spec_from_file_location(mod_name, fname)
#mod = imputil.module_from_spec(spec)
#spec.loader.exec_module(mod)
mod.PoEHelper().run()
