# -*- mode: python -*-

block_cipher = None


a = Analysis(['exeloader.pyw'],
        pathex=[],
        binaries=[],
        datas=[(r'data\doc', r'data\doc'), (r'data\rsrc', r'data\rsrc'), (r'data\plugins', r'data\plugins'), ],
        hiddenimports=['PySide2', 'cv2', 'numpy', 'PIL', 'requests', 'keyboard', 'mouse', 'tkinter'],
        hookspath=[],
        runtime_hooks=[],
        excludes=['poehelper', 'iolib', 'iopoelib'],
        win_no_prefer_redirects=False,
        win_private_assemblies=False,
        cipher=block_cipher,
        noarchive=False
        )
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)
exe = EXE(pyz,
        a.scripts,
        [],
        exclude_binaries=True,
        name='poehelper',
        debug=False,
        bootloader_ignore_signals=False,
        strip=False,
        upx=True,
        console=False 
        )
coll = COLLECT(exe,
        a.binaries,
        a.zipfiles,
        a.datas,
        strip=False,
        upx=True,
        name='poehelper'
        )
