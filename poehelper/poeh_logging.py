"""
PoEHelper - logging API.

Author: Vlad Topan (vtopan/gmail)
"""

import threading
import time


DEBUG = False
LOG_FILE = 'poehelper.log'
LOG_LOCK = threading.Lock()
LOG_CALLBACK = None


def out(msg):
    """
    Outputs a message to the log. Internal function; use `log()`, `err()` or `dbg()`.

    The UI will display the contents of the log as well.
    """
    if LOG_CALLBACK:
        if LOG_CALLBACK(msg):
            return
    if LOG_FILE:
        LOG_LOCK.acquire()
        try:
            with open(LOG_FILE, 'a') as fh:
                fh.write(time.strftime('[%y.%m.%d-%H:%M:%S] ') + str(msg) + '\n')
        finally:
            LOG_LOCK.release()
    else:
        print(msg)


def log(msg):
    """
    Logs a message.
    """
    out(f'[*] {msg}')


def err(msg):
    """
    Logs a warning or error.
    """
    out(f'[!] {msg}')


def dbg(msg):
    """
    Logs a debug message (if debugging is enabled).
    """
    if DEBUG:
        out(f'[#] {msg}')


def set_debugging(enabled):
    """
    Enable/disable debugging messages.
    """
    global DEBUG
    DEBUG = bool(enabled)


def get_log_file():
    """
    Get the log file.
    """
    return LOG_FILE
    
    
def set_log_file(logfile):
    """
    Set the log file.
    """
    global LOG_FILE
    LOG_FILE = logfile


def set_log_callback(fun):
    global LOG_CALLBACK
    LOG_CALLBACK = fun


