"""
PoEHelper plugin manager.

Author: Vlad Topan (vtopan/gmail)
"""

import glob
import importlib.util as imputil
import os

from .poeh_plugins import PoehPlugin, PlugApi
from .poeh_utils import adict, hotkey, brief_exc
from .poeh_logging import log, err, dbg


class PlugMgr:
    """
    Plugin manager.
    """

    def __init__(self, app):
        self.app = app
        self.api = PlugApi(mgr=self)
        self.plugins = []
        for path in ('plugins', 'local_plugins'):
            self.load_plugins(app.cfg.paths[path])
        self._dirty = False  # set when a new plugin is loaded
        self._evt_handlers = {}


    def load_plugin(self, plugin_class, filename):
        """
        Load the plugin with the given name and base class.
        """
        name = plugin_class.__name__
        if name not in self.app.cfg_plugins:
            self.app.cfg_plugins[name] = adict()
        cfg = self.app.cfg_plugins[name]
        # add the plugin's default cfg options where missing
        for k, v in getattr(plugin_class, 'CFG', {}).items():
            if k not in cfg:
                cfg[k] = v
        # add the 'game' section of the global config
        cfg['game'] = dict(self.app.cfg.game.items())
        # create the plugin instance and register
        try:
            plugin = plugin_class(paths=self.app.cfg.paths, cfg=cfg, api=self.api)
        except Exception as e:
            err(f'Plugin {name} failed to initialize: {brief_exc(e)}!')
            return
        plugin.name = name
        plugin.file_path = filename
        plugin.evt_filters = tuple(getattr(plugin_class, 'EVT_FILTERS', []))
        self.plugins.append(plugin)
        self._dirty = True
        dbg(f'Loaded plugin [{name}].')


    def load_plugins(self, path):
        """
        Load the plugins in the given folder (usually the `<proj>/plugins` subfolder).
        """
        plugins = self.plugins
        for f in glob.glob(f'{path}/*_plugin.py'):
            mod_name = os.path.basename(f).rsplit('.', 1)[0]
            spec = imputil.spec_from_file_location(mod_name, f)
            mod = imputil.module_from_spec(spec)
            try:
                spec.loader.exec_module(mod)
            except Exception as e:
                err(f'Plugin file {f} failed to load: {brief_exc(e)}!')
                continue
            for clsname in dir(mod):
                # look for plugin classes in the module's contents
                if clsname == 'PoehPlugin' or clsname[0] == '_' or not clsname.endswith('Plugin'):
                    continue
                cls = getattr(mod, clsname)
                if not issubclass(cls, PoehPlugin):
                    continue
                dbg(f'[*] Found plugin {clsname} (module: {mod_name})')
                self.load_plugin(cls, f)
        return plugins


    def _filtering_hotkey_callback(self, callback, args=None):
        """
        Internal wrapper used to only execute hotkeys if the game is focused.
        """
        args = args or []

        def _f():
            if not self.app.evt_mgr.game_focused:
                return True
            return callback(*args)
        return _f


    def add_game_hotkey(self, key, callback, args=None, suppress=False):
        """
        Add a game hotkey for a plugin.
        """
        hotkey(key, self._filtering_hotkey_callback(callback, args), suppress=suppress)


    def event_handlers(self, etype):
        """
        Lazily generates the list of plugins which want to handle an event.
        """
        if etype not in self._evt_handlers or self._dirty:
            etypes = set(self._evt_handlers.keys()) | set([etype])
            for et in etypes:
                self._evt_handlers[et] = [(getattr(p, f'{et}_handler', None), p)
                        for p in self.plugins]
                self._evt_handlers[et] = [x for x in self._evt_handlers[et] if x[0]]
            self._dirty = False
        return self._evt_handlers[etype]


    def handle_event(self, etype, earg, **kwargs):
        """
        Pass an event to plugins that have registered handlers.
        """
        for fun, p in self.event_handlers(etype):
            filters = p.evt_filters
            if (not self.app.evt_mgr.game_focused) and 'game_focused' in filters:
                continue
            try:
                fun(earg, **kwargs)
            except Exception as e:
                err(f'Plugin {p.name} failed handling event [{etype}]: {brief_exc(e)}!')


