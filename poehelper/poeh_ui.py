#!/usr/bin/env python3
"""
PoEHelper - UI.

Author: Vlad Topan (vtopan/gmail)
"""

import cgi
import os
import re
import time

from PySide2 import QtGui, QtWidgets
from PySide2.QtCore import Qt, SIGNAL, QCoreApplication, Signal, Slot, QTimer
import keyboard

from .poeh_constants import LOG_COLOR_MAP
from .poeh_logging import log, err, dbg
from .poeh_utils import adict, hotkey, StoppableTimer, format_seconds
from .poeh_ui_components import UiIndicator, Button, ToggleGroup, PluginPanel


class PoehMainWindow(QtWidgets.QMainWindow):
    """
    PoE Helper main window.

    :param app: The PoEHelper (application) instance.
    """

    set_ui_text_sig = Signal(str, str)
    toggle_hide_sig = Signal()


    def __init__(self, app):
        # todo: split properly
        super().__init__()
        self.app = app
        self.qtapp = QCoreApplication.instance()
        self.cfg = app.cfg.ui
        self.hidden = False
        # settings
        self.load_resources()
        # UI components
        self.main = QtWidgets.QWidget()     # central widget
        self.lbl = adict()  # labels
        self.ind = adict()  # indicators
        self.but = adict()  # buttons
        self.ind.game_running = UiIndicator(0)
        self.ind.afk = UiIndicator(0, colors=('black', '#AA0'), labels=('', 'afk'), hide_when_off=1)
        self.lbl.area = QtWidgets.QLabel('unknown area')
        if self.cfg.enable_timer:
            self.but.timer = QtWidgets.QPushButton('00:00', self)
            self.but.timer.setFixedSize(60, 24)
            self.but.timer.clicked.connect(self.timer_clicked)
            self.timer = None
        if self.cfg.enable_counter:
            self.but.counter = QtWidgets.QPushButton('0', self)
            self.but.counter.setFixedSize(30, 24)
            self.but.counter.clicked.connect(self.counter_clicked)
            self.but.counter.setContextMenuPolicy(Qt.CustomContextMenu)
            self.but.counter.customContextMenuRequested.connect(self.counter_reset)
            self.counter = 0
        self.but.min = Button('-', self, width=20, height=20)
        self.but.min.clicked.connect(self.toggle_hide)
        self.eventlog = QtWidgets.QTextEdit('')
        self.eventlog.setStyleSheet('QScrollBar:vertical {width: 10px;}')
        self.eventlog.setPlaceholderText('Log messages')
        self.eventlog.setReadOnly(1)
        self.refdesc, self.lay_refdesc = [], QtWidgets.QHBoxLayout()
        self.refdesc.append(QtWidgets.QLabel('Search:'))
        self.refdesc.append(QtWidgets.QLineEdit())
        self.refdesc[-1].textChanged.connect(self.reference_search)
        self.ref_search_timer = None
        self.reference = ToggleGroup('Reference', description=self.lay_refdesc,
                notify_on_resize=self, cols=3)
        for e in self.refdesc:
            self.lay_refdesc.addWidget(e)
        self.open_ref_panels = []
        self.plugins = ToggleGroup('Plugins', notify_on_resize=self)
        # layout
        self.lay = QtWidgets.QVBoxLayout()
        self.lay.setContentsMargins(5, 5, 5, 5)
        self.lay_title = QtWidgets.QHBoxLayout()
        self.lay_title.addWidget(self.ind.game_running)
        self.lay_title.addWidget(self.ind.afk)
        self.lay_title.addWidget(self.lbl.area)
        if self.cfg.enable_counter:
            self.lay_title.addWidget(self.but.counter)
        if self.cfg.enable_timer:
            self.lay_title.addWidget(self.but.timer)
        self.lay_title.addWidget(self.but.min)
        self.lay.addLayout(self.lay_title)
        self.lay.addWidget(self.reference)
        self.lay.addWidget(self.plugins)
        self.lay.addWidget(self.eventlog)
        self.main.setLayout(self.lay)
        self.setCentralWidget(self.main)
        # shortcuts
        self.connect(QtWidgets.QShortcut(QtGui.QKeySequence("Esc"), self), SIGNAL('activated()'),
                self.hide_app)
        self.connect(QtWidgets.QShortcut(QtGui.QKeySequence("Alt+Q"), self), SIGNAL('activated()'),
                self.close)
        self.connect(QtWidgets.QShortcut(QtGui.QKeySequence("F"), self), SIGNAL('activated()'),
                self.refdesc[-1].setFocus)
        QtWidgets.QShortcut(QtGui.QKeySequence("Alt+F2"), self, lambda:1)
        QtWidgets.QShortcut(QtGui.QKeySequence("Ctrl+C"), self, lambda:1)
        hotkey('Alt+F2', self.toggle_hide)
        # window setup
        self.autosize()
        self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
        self.statusBar().setSizeGripEnabled(True)
        if 'x' in self.cfg:
            self.move(self.cfg.x, self.cfg.y)
        else:
            # center horizontally at the top of the screen
            rect = self.frameGeometry()
            pos = QtWidgets.QDesktopWidget().availableGeometry().center()
            pos.setY(0)
            rect.moveCenter(pos)
            self.move(pos.x(), pos.y())
        self.set_ui_text_sig.connect(self.set_ui_text_slot)
        self.toggle_hide_sig.connect(self.toggle_hide_slot)
        rowheight = QtGui.QFontMetrics(self.eventlog.font()).lineSpacing()
        self.eventlog.setFixedHeight(10 + 3 * rowheight)
        self.eventlog.setFocus()
        self.next_expand_size = None
        self.was_in_town_or_ho = False
        self.was_hidden = False
        self.reference.autosize()
        self.autosize()
        self.refdesc[-1].setFocus()
        self.plugin_panels = []


    def counter_clicked(self):
        """
        Increment the counter.
        """
        self.counter += 1
        self.but.counter.setText(str(self.counter))


    def counter_reset(self):
        """
        Reset the counter.
        """
        self.counter = 0
        self.but.counter.setText(str(self.counter))


    def timer_clicked(self):
        """
        Start/reset the timer.
        """
        if self.timer:
            log(f'Timer restarted after {format_seconds(self.timer.restart())}.')
        else:
            self.timer = StoppableTimer(callback=self.set_ui_text, cbkargs=('but.timer',))
            self.timer.start()
            dbg('Starting timer...')


    def load_resources(self):
        """
        Load app resources from files (fonts, CSS, etc.)
        """
        cfg = self.app.cfg
        self.font = load_font(f'{cfg.paths.rsrc}/fonts/Fontin-Regular.ttf')
        self.font.setPointSize(9)
        self.qtapp.setFont(self.font)
        css = open(f'{cfg.paths.rsrc}/poehelper.css').read()
        if os.path.isfile(cfg.paths.user_css_file):
            css += open(cfg.paths.user_css_file)
        self.setStyleSheet(css)


    def mousePressEvent(self, evt):
        """
        Hook this to allow dragging by clicking anywhere.
        """
        self.drag_base_pos = evt.pos()


    def mouseReleaseEvent(self, evt):
        """
        Hook this to allow dragging by clicking anywhere.
        """
        self.drag_base_pos = None


    def mouseMoveEvent(self, evt):
        """
        Hook this to allow dragging by clicking anywhere.
        """
        if getattr(self, 'drag_base_pos', None) and evt.buttons() & Qt.LeftButton:
            diff = evt.pos() - self.drag_base_pos
            self.move(self.pos() + diff)


    def add_reference_panel(self, name, description=None, panels=None, cols=4):
        """
        Add a panel to the reference group.
        """
        widgets = {}
        for k, v in panels.items():
            if not v:
                widgets[k] = None
                continue
            if type(v) is list:
                v = '<br>'.join(v)
            w = QtWidgets.QTextBrowser()
            w.setOpenExternalLinks(True)
            w.setStyleSheet('a:hover {color:#AA0;text-decoration:none;}')
            w.setFocusPolicy(Qt.NoFocus)
            w.setHtml(f'<center><b>{k}</b></center>' + v.strip())
            widgets[k] = w
        if description:
            description = QtWidgets.QLabel(description)
        subpanel = ToggleGroup(name, description=description, panels=widgets, cols=cols,
                notify_on_resize=self.reference)
        self.reference.add_panel(name, subpanel)
        subpanel.autosize()
        for w in widgets.values():
            if not w:
                continue
            linecount = w.toPlainText().count('\n') + 1
            rowheight = QtGui.QFontMetrics(w.font()).lineSpacing()
            height = 10 + linecount * rowheight
            w.setFixedHeight(height)


    def show_reference_panel(self, panel, subpanel=None, show=True):
        """
        Show (open) a reference panel / subpanel.
        """
        if show or not subpanel:
            self.reference.show_panel(panel, show=show)
            if show == False:
                self.reference.panels[panel].hide_all_panels()
        if subpanel:
            self.reference.panels[panel].show_panel(subpanel, show=show)


    def reference_search(self):
        """
        Search references for text.
        """
        if not self.ref_search_timer:
            self.ref_search_timer = QTimer(self)
            self.ref_search_timer.timeout.connect(self._reference_search)
            self.ref_search_timer.setSingleShot(True)
        self.ref_search_timer.setInterval(500)
        if not self.ref_search_timer.isActive():
            self.ref_search_timer.start()


    def _reference_search(self):
        self.ref_search_timer = None
        text = self.refdesc[-1].text().lower().strip()
        if not text:
            return
        open_ref_panels = []
        for pname, panel in self.reference.panels.items():
            for k, v in panel.panels.items():
                if text in str(v.toPlainText()).lower():
                    open_ref_panels.append((pname, k))
        if open_ref_panels != self.open_ref_panels:
            for p, sp in self.open_ref_panels:
                if (p, sp) not in open_ref_panels:
                    self.show_reference_panel(p, sp, show=False)
            for p, sp in open_ref_panels:
                if (p, sp) not in self.open_ref_panels:
                    self.show_reference_panel(p, sp, show=True)
            self.open_ref_panels = open_ref_panels


    def create_plugin_panel(self, title, text_ui):
        """
        Creates a plugin's panel based on the layout described in `text_ui`.
        """
        panel = PluginPanel(title, fields_as_str=text_ui)
        self.plugins.add_panel(title, panel)
        return panel


    def autosize(self):
        """
        Resize the window.
        """
        # self.setFixedSize(self.cfg.width, self.minimumSizeHint().height())
        # hack: wait for potential resize event to propagate to layouts
        for i in range(10):
            self.qtapp.processEvents()
        self.resize(self.cfg.width, self.sizeHint().height())


    def raise_app(self):
        """
        Bring the application into focus.
        """
        if self.hidden:
            self.toggle_hide()
        # self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint)
        # self.show()
        return True


    def hide_app(self):
        """
        Hide the app.
        """
        if not self.hidden:
            self.toggle_hide()
        # self.setWindowFlags(self.windowFlags() & ~Qt.WindowStaysOnTopHint)
        return True


    def toggle_hide(self):
        self.toggle_hide_sig.emit()


    @Slot()
    def toggle_hide_slot(self):
        """
        Hide or show the app.
        """
        hide = not self.hidden
        self.eventlog.setVisible(not hide)
        self.reference.setVisible(not hide)
        self.plugins.setVisible(not hide)
        self.statusBar().setVisible(not hide)
        if self.next_expand_size:
            height = self.next_expand_size
            self.next_expand_size = None
        else:
            height = self.cfg.height
        self.setMaximumHeight(40 if hide else 100000)
        self.hidden = hide
        self.autosize()
        # if hide:  # fixme!!!
        #    keyboard.press_and_release('alt+tab')


    def close(self):
        """
        Close the GUI.
        """
        if self.timer:
            self.timer.stop = 1
        self.app.close()


    def run(self):
        """
        Start the GUI main loop.
        """
        self.qtapp.exec_()


    def set_ui_text(self, node, value, cat=None):
        """
        Allows setting UI elements from other threads (signal/slot mechanism).
        """
        if cat:
            node = f'{cat}.{node}'
        value = str(value)
        self.set_ui_text_sig.emit(node, value)


    @Slot(str, str)
    def set_ui_text_slot(self, node, value):
        """
        Slot for the .set_ui_text_sig() signal.
        """
        if '.' in node:
            cat, node = node.split('.', 1)
            obj = getattr(self, cat)[node]
            if cat in ('lbl', 'but'):
                obj.setText(value)
            elif cat == 'ind':
                obj.setIndState(int(value))
            else:
                raise ValueError(f'Unknown category [{cat}]!')
        elif node == 'eventlog':
            if self.cfg.log_timestamps:
                value = f"<font color='#432'>{time.strftime('%H:%M:%S')}</font> {value}"
            self.eventlog.append(value)
            self.eventlog.ensureCursorVisible()
        else:
            raise ValueError(f'Unknown UI component [{node}]!')


    def handle_event(self, etype, earg, **kwargs):
        """
        Handle app event.
        """
        if etype == 'area_changed':
            self.set_ui_text('lbl.area', f'{earg}')
            hide = None
            if (kwargs['in_town'] or kwargs['in_hideout']) and self.cfg.auto_show and not self.was_in_town_or_ho:
                hide = self.was_hidden
            elif not (kwargs['in_town'] or kwargs['in_hideout']) and self.cfg.auto_hide:
                self.was_hidden = self.hidden
                hide = True
            if hide is not None and hide != self.hidden:
                self.toggle_hide_sig.emit()
            if self.cfg.auto_collapse_reference:
                for panel in self.reference.panels.values():
                    panel.hide_all_panels()
            self.was_in_town_or_ho = kwargs['in_town'] or kwargs['in_hideout']
        elif etype in ('afk', 'game_running'):
            self.set_ui_text(f'ind.{etype}', int(earg))
        elif etype == 'pm':
            if not self.cfg.show_pms:
                return
            msg = (earg[:70] + '[...]') if len(earg) > 70 else earg
            msg = f'PM <font color=red>{cgi.escape(kwargs["sender"])}</font>: {cgi.escape(msg)}'
            self.set_ui_text('eventlog', msg)
        elif etype == 'ingame_ctrlc':
            self.set_ui_text('eventlog', f'Ctrl+C: {earg["types"][0]} - '
                    f'{earg.get("name", earg["types"][1])}')
        else:
            # ignore unknown events
            pass
            # msg = f'<font color=blue>{etype}</font>: <b>{earg}</b> ({kwargs})'
            # self.set_ui_text('eventlog', msg)


    def msg(self, msg):
        """
        Show message in event log.
        """
        if self.cfg.ignore_dbg_messages and msg.startswith('[#] '):
            return
        msg = re.sub(r'^\[([!#*])\] (.+)',
                lambda m:f'<font color="{LOG_COLOR_MAP[m[1]]}">{m[2]}</font>', msg)
        self.set_ui_text('eventlog', msg)



def create_ui(app):
    qtapp = QtWidgets.QApplication()
    qtapp.setApplicationName('PoE Helper')
    window = PoehMainWindow(app=app)
    window.show()
    return window


def load_font(font_file):
    """
    Load a font from a (TTF/OTF) file and return it.
    """
    db = QtGui.QFontDatabase()
    font_id = db.addApplicationFont(font_file)
    name = db.applicationFontFamilies(font_id)[0]
    font = QtGui.QFont(name)
    dbg(f'Loaded font [{name}] from file [{font_file}]')
    return font

