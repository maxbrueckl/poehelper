#!/usr/bin/env python3
"""
PoEHelper - misc function library.

Author: Vlad Topan (vtopan/gmail)
"""

import threading
from tkinter import Tk, TclError  # Windows only, for easy clipboard access
import time
import traceback

from PIL import ImageGrab
import keyboard

from iolib.win import get_window_rect, FindWindow



class adict(dict):
    """
    Attribute dict - contents are available through attributes as well as through keys.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dict__ = self


def read_clipboard():
    """
    Get Windows clipboard contents.
    """
    try:
        text = Tk().clipboard_get()
    except TclError:     # ignore non-text clipboard contents
        text = None
    return text


def hotkey(key_name, handler, args=None, suppress=True, timeout=0):
    """
    Hooks a keyboard event ("hotkey").

    :param key_name: String describing the shortcut (e.g. 'ctrl+a').
    :param handler: Callback function handling the event, will receive the `arg` argument.
    :param arg: Argument passed to the callback.
    """
    keyboard.add_hotkey(key_name, handler, args=args) # , suppress=suppress)


def brief_exc(exc, html=True):
    """
    Return a brief text describing the last exception.
    """
    location = traceback.format_exc().strip().split('\n')[-3]
    location = location.split('\\')[-1].replace('", line ', ':')
    estring = f'{exc.__class__.__name__}: {exc} @ {location}'
    if html:
        estring = estring.replace('<', '&lt;').replace('>', '&gt;')
    return estring


    
def game_hwnd():
    """
    Return the HWND of the game window (or None).
    """
    return FindWindow(None, 'Path of Exile')
    
    
def game_screenshot():
    """
    Takes a screenshot of the game window.
    
    :return: A PIL image.
    """
    rect = get_window_rect('Path of Exile')
    img = ImageGrab.grab(tuple(rect))
    return img


def format_seconds(t):
    """
    Format a time in seconds as a "[H:]:MM:SS" string.
    """
    res = '%02d:%02d' % ((t // 60) % 60, t % 60)
    if t > 3600:
        res = f'{t//3600}:{res}'
    return res



class StoppableTimer(threading.Thread):


    def __init__(self, callback, cbkargs, *args, **kwargs):
        super().__init__()
        self.stop = 0
        self.elapsed = 0
        self.callback = callback
        self.cbkargs = cbkargs


    def run(self):
        self.start_time = time.time()
        while not self.stop:
            self.elapsed = int(time.time() - self.start_time)
            args = self.cbkargs + (format_seconds(self.elapsed),)
            self.callback(*args)
            time.sleep(0.5)


    def restart(self):
        """
        Restart timer and return elapsed time.
        """
        elapsed = self.elapsed
        self.start_time = time.time()
        return elapsed

