"""
PoEHelper: Path of Exile pluginable overlay helper.

Author: Vlad Topan (vtopan/gmail)
"""

import copy
import json
import os
import sys
import time

from .poeh_events import EventMgr
from .poeh_logging import log, err, dbg, set_log_callback, get_log_file, set_log_file
from .poeh_plugmgr import PlugMgr
from .poeh_ui import create_ui
from .poeh_utils import adict


PROJ_PATH = os.path.dirname(os.path.dirname(__file__))
if PROJ_PATH not in sys.path:
    sys.path += [PROJ_PATH, rf'{PROJ_PATH}\lib']



class PoEHelper():
    """
    Main program class.
    """


    def __init__(self):
        self.load_cfg(init=True)
        if not os.path.isdir(self.cfg.paths.out):
            os.makedirs(self.cfg.paths.out)
        self.ui = create_ui(app=self)
        set_log_callback(self.ui.msg)
        if get_log_file() == 'poehelper.log':
            set_log_file(rf'{self.cfg.paths.out}\poehelper.log')
        self.plug_mgr = PlugMgr(app=self)
        self.evt_mgr = EventMgr(app=self)
        self.save_cfg()
        dbg(f'Program started @ {time.strftime("%H:%M:%S, %d.%m.%Y")}')


    def close(self):
        """
        Close the app.
        """
        self.evt_mgr.close()
        dbg(f'Program closed @ {time.strftime("%H:%M:%S, %d.%m.%Y")}')
        sys.exit()


    def run(self):
        """
        Run the app.
        """
        self.ui.run()


    def save_cfg(self):
        """
        Save the configuration file.
        """
        self.cfg.ui.update(dict(zip(('x', 'y'), self.ui.pos().toTuple())))
        json.dump(self.cfg, open(self.cfg_file, 'w'), indent=2, sort_keys=1)
        pcfg = copy.deepcopy(self.cfg_plugins)
        for p, v in pcfg.items():
            if 'game' in v:
                del v['game']
        json.dump(pcfg, open(self.plug_cfg_file, 'w'), indent=2, sort_keys=1)


    def load_cfg(self, init=True):
        """
        Load the configuration file.
        """
        if init:
            cfg = self.cfg = adict()
            self.cfg_plugins = adict()
            paths = cfg.paths = adict(proj=os.path.dirname(os.path.dirname(__file__)))
            if paths.proj not in sys.path:
                sys.path.append(paths.proj)
            paths.data = rf'{paths.proj}\data'    
            if not os.path.isdir(paths.data):
                paths.data = paths.proj
            paths.plugins = os.path.join(paths.data, 'plugins')
            paths.local = os.path.join(paths.proj, '.local')
            paths.local_plugins = os.path.join(paths.local, 'plugins')
            paths.out = os.path.join(paths.data, 'out')
            paths.doc = os.path.join(paths.data, 'doc')
            paths.rsrc = os.path.join(paths.data, 'rsrc')
            paths.user_css_file = os.path.join(paths.rsrc, 'user_style.css')
            paths.data_cache = os.path.join(paths.rsrc, 'data-cache')
            paths.game_docs = os.path.expandvars(r'%USERPROFILE%\Documents\My Games\Path of Exile')
            paths.screenshots = paths.game_docs + r'\Screenshots'
            cfg.ui = adict(width=400, height=200, ignore_dbg_messages=True, auto_hide=True,
                    auto_show=True, show_pms=True, auto_collapse_reference=False,
                    enable_timer=True, enable_counter=True, log_timestamps=False)
            cfg.options = adict(watch_client_txt=True, poll_frequency=0.1, log_tail=-1)
            cfg.plugins = adict()
            cfg.game = adict(playername=None, league='Betrayal')
            self.cfg_file = f'{self.cfg.paths.data}/config.json'
            self.plug_cfg_file = f'{self.cfg.paths.data}/config_plugins.json'
        if os.path.isfile(self.plug_cfg_file):
            self.cfg_plugins = json.load(open(self.plug_cfg_file))
        if not os.path.isfile(self.cfg_file):
            if not init:
                raise ValueError(f'Missing configuration file [{self.cfg_file}]!')
            return self.cfg
        if not os.path.getsize(self.cfg_file):
            return self.cfg
        for k, v in json.load(open(self.cfg_file)).items():
            if k not in self.cfg and k not in ('plugins',):
                self.cfg[k] = adict()
            for kk, vv in v.items():
                # only override fields present in the config file
                self.cfg[k][kk] = vv
        return self.cfg


